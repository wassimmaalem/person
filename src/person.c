/*
 * main.c
 *
 *  Created on: Apr 15, 2020
 *      Author: ITSWASSIM
 */
#include "../include/person.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MALLOC(ptr,size) 		\
		do{						\
			ptr=malloc(size);	\
			if (!ptr)			\
					abort();	\
		}while(0)

#define FREE(ptr)				\
		do{						\
			free(ptr);			\
			ptr=NULL;			\
		}while(0)

static const int BUFFER_SIZE=1024;

#define READ_MODE "r"

static struct PersonRepository {
	int initialized;
	TAILQ_HEAD(p_list, Person) head;
}person_repository = {.initialized=0, .head=NULL};

int person_init(){
	TAILQ_INIT(&person_repository.head);
	person_repository.initialized=1;
	return EXIT_SUCCESS;
}

int person_deinit(){
	struct Person *p;
	while((p=person_remove_head())){
			FREE(p->name);
			FREE(p->died);
			FREE(p);
	}
	return EXIT_SUCCESS;
}

/**
 * \brief str_chomp remove excess space outside of a word
 *
 * cut out the CR and LF from the end of a line to avoid using unnecessary space in the file
 *
 * \return beginning of the line
 */
char *str_chomp(char *line) {
	if (!(line && *line))
				return line;

	char *begin=line;
	while (*line++)
			;

	while (--line>=begin) {
		if (*line >= ' ')
				return begin;

		if (*line == '\r' || *line == '\n')
				*line = '\0';
	}

	return begin;
}

struct Person *person_create(char* name, int age, int statement, char* died) {
		if (!(name && *name && died && *died && age>0 && age<MAX_AGE
				&& statement >= SINGLE
				&& statement <= UNKNOWN))
				return NULL;

		struct Person *result;
		MALLOC(result, sizeof(struct Person));
		result->name = name;
		result->age=age;
		result->state=statement;
		result->died=died;
		return result;
}

/**
 * \brief process person from the line of a file
 *
 * \return NULL if there's no line
 *
 * \return result if everything goes accordingly
 *
 * Implement STRICT if there's all the parameters needed (name, age, state and died)
 *
 * \return only the name if there's no other parameter
 */
struct Person *person_process_file_line(char *line) {
		if (!(line && *line))
			return NULL;

		struct Person *result=NULL;
		char *n = strtok(line, ";");
		char *a = strtok(NULL, ";");
		char *s = strtok(NULL, ";");
		char *d = strtok(NULL, ";");

#ifdef STRICT
		if (n && *n && a && *a && s && *s && d && *d)
				result = person_create(strdup(n),
									atoi(a),
									atoi(s),
									strdup(d));
#else
		if (n && *n)
				result = person_create(strdup(n),
							a ? atoi(a) : 0,
							s ? atoi(s) : 0,
							d ? strdup(d) : "?");
#endif
		return result;
}

int person_counts() {
	if(!person_repository.initialized)
		return 0;

	struct Person *p = NULL;
	int result=0;
	TAILQ_FOREACH(p, &person_repository.head, next) {
		++result;
	}
	return result;
}

int person_load_from_file(const char *name){
	if (!(name && *name))
			return EXIT_FAILURE;

	FILE *f=fopen(name, READ_MODE);
	if (!f)
			return EXIT_FAILURE;

	char *line;
	MALLOC(line, BUFFER_SIZE);

	struct Person *person=NULL;

	while((line=fgets(line, BUFFER_SIZE, f))) {
			person = person_process_file_line(str_chomp(line));
			person_add(person);
	}

	FREE(line);
	fclose(f);
	return EXIT_SUCCESS;
}

int person_add(struct Person *person){
		if(!(person_repository.initialized && person))
				return EXIT_FAILURE;

		TAILQ_INSERT_TAIL(&person_repository.head, person, next);
		return EXIT_SUCCESS;
}

struct Person *person_remove_head(){
		if (!(person_repository.initialized))
				return NULL;

		struct Person *result=TAILQ_FIRST(&person_repository.head);

		if (result)
			TAILQ_REMOVE(&person_repository.head, result, next);

		return result;
}

char *person_get_state(const struct Person *person){
		if(!person)
			return "";
		switch(person->state) {
		case SINGLE : return "Single";
		case DIVORCED : return "Divorced";
		case MARRIED : return "Married";
		case WIDOW : return "Widow";
		case UNKNOWN : return "Unknown";
		default : return "?";
		}
}

void person_print_a_person(const struct Person *person){
		if (!person)
			return;

		printf("Name:%s, Age:%d, State:%s, Died at:%s\n",
					person->name, person->age,
					person_get_state(person),
					person->died);
}

void person_print_all(){
		if(!person_repository.initialized)
			return;

		struct Person *p;
		TAILQ_FOREACH(p, &person_repository.head, next){
			person_print_a_person(p);
		}
}

struct Person *person_find_by_name(const char *name, struct Person *from){
		if (!(name && *name && from))
				return NULL;

		struct Person *it=from;

		while ( (it=TAILQ_NEXT(it, next)) ){
				if (strcmp(name, it->name) == 0)
					return it;
		}

		return NULL;
}

struct Person *person_find_by_age(int age, struct Person *from){
		struct Person *fa=from;

		TAILQ_FOREACH(fa, &person_repository.head, next){
				if (fa->age==age)
					return (fa);
		}
		return NULL;
}

struct Person *person_first(){
		return person_repository.initialized
					? TAILQ_FIRST(&person_repository.head)
				: NULL;
}

struct Person *person_last(){
	return person_repository.initialized
			? TAILQ_LAST(&person_repository.head, p_list)
			: NULL;
}

int compare_by_name(struct Person *a, struct Person *b){
	if (a==b)
		return 0;
	if (!a && b)
		return 1;
	if (a && !b)
		return -1;

	return strcmp(a->name, b->name);
}

void person_bubble_sort(enum Ordering ordering,
						int (*comparer)(struct Person *a, struct Person *b)){
	if (comparer==NULL)
			return;

	int change=0;
	do {
			change = 0;
			struct Person *p1=TAILQ_FIRST(&person_repository.head);
			struct Person *p2=TAILQ_NEXT(p1, next);
			while (p1 && p2) {
				int result = comparer(p1, p2);
				if (((ordering==ASCENDING) && (result>0))
					|| ((ordering==DESCENDING) && (result<0))) {
					TAILQ_REMOVE(&person_repository.head, p1, next);
					TAILQ_INSERT_AFTER(&person_repository.head, p2, p1, next);
					change++;
					p2=TAILQ_NEXT(p1, next);
				}
				else {
					p1=p2;
					p2=TAILQ_NEXT(p2, next);
				}
			}
		}while(change);
 }

int person_compare_by_name(const struct Person *a, const struct Person *b){
		if (a==b)
			return 0;
		if (!a && b)
			return 1;
		if (a && !b)
			return -1;

		return strcmp(a->name, b->name);
}

int person_compare_by_age(const struct Person *a, const struct Person *b){
			if (a==b)
				return 0;
			if (!a&&b)
				return -1;
			if (a&&!b)
				return 1;

			return (a->age-b->age);

}

int person_compare_by_state(const struct Person *a, const struct Person *b){
			if (a==b)
				return 0;
			if (!a&&b)
				return -1;
			if (a&&!b)
				return 1;

			return (a->state-b->state);
}

int person_compare_by_died(const struct Person *a, const struct Person *b){
			if (a==b)
				return 0;
			if (!a && b)
				return -1;
			if (a && !b)
				return 1;

			return strcmp(a->died, b->died);
}
