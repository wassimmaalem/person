/*
 * main.c
 *
 *  Created on: Apr 15, 2020
 *      Author: ITSWASSIM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <CUnit/Basic.h>

#include "../include/person.h"

#define NUMBER_OF_PERSON 30

char *NAME="Jack";
char *DIED="Brell";

#define AGE 45

const enum PersonState STATE = WIDOW;

#define MALLOC(ptr,size) 		\
		do{						\
			ptr=malloc(size);	\
			if (!ptr)			\
					abort();	\
		}while(0)

#define FREE(ptr)				\
		do{						\
			free(ptr);			\
			ptr=NULL;			\
		}while(0)

char NO_NAME[]="Unidentifiable Person";

int init_test(void){
	person_init();
	return (0);
}

int clean_test(void){
	person_deinit();
	return (0);
}

void test_create_person() {
	struct Person *p=NULL;
	//Normal behavior
	p = person_create(NAME, AGE, STATE, DIED);
	CU_ASSERT_PTR_NOT_NULL(p);
	CU_ASSERT_STRING_EQUAL(NAME, p->name);
	CU_ASSERT_EQUAL(STATE, p->state);
	CU_ASSERT_EQUAL(AGE, p->age);
	CU_ASSERT_STRING_EQUAL(DIED, p->died);
	//Abnormal behavior
	p = person_create(NAME, -1, STATE, "");
	CU_ASSERT_PTR_NULL(p);
	p = person_create("", AGE, -1, DIED);
	CU_ASSERT_PTR_NULL(p);
	p = person_create(NULL, AGE, STATE, DIED);
		CU_ASSERT_PTR_NULL(p);
	p = person_create(NAME, AGE, STATE, "");
		CU_ASSERT_PTR_NULL(p);
	CU_PASS("Person created successfully");
}

void test_add_person() {
	for (int i=0; i<NUMBER_OF_PERSON; i++){
			char *name=NULL;
			asprintf(&name, "%s, %d", NO_NAME, i);
			struct Person *p = person_create(name, (int)i+1, (enum PersonState) (i % UNKNOWN), "died");
			CU_ASSERT_EQUAL(person_add(p), EXIT_SUCCESS);
	}

	CU_ASSERT_EQUAL(person_counts(), NUMBER_OF_PERSON);
	for (int i=0; i<NUMBER_OF_PERSON; i++) {
		struct Person *p = person_remove_head();
		CU_ASSERT_PTR_NOT_NULL(p);
	}

	CU_ASSERT_PTR_NULL(person_remove_head());
	CU_ASSERT_EQUAL(person_counts(), 0);

	CU_ASSERT_EQUAL(person_add(NULL), EXIT_FAILURE);
	CU_PASS("Person added successfully");
}

void test_person_first(){
	struct Person *person_first;
	CU_ASSERT_PTR_NOT_NULL(person_first);
	//Normal Behavior
	CU_ASSERT_EQUAL(person_init(), EXIT_SUCCESS);
	CU_PASS("First Person found");
}

void test_person_last(){
	struct Person *person_last;
	CU_ASSERT_PTR_NOT_NULL(person_last);
	//Normal Behavior
	CU_ASSERT_EQUAL(person_init(), EXIT_SUCCESS);
	CU_PASS("Last Person found");
}

void test_read_file() {
		// Normal Behavior
		CU_ASSERT_EQUAL(person_load_from_file("vh.txt"), EXIT_SUCCESS);
		//Abnormal Behavior
		CU_ASSERT_EQUAL(person_load_from_file(""), EXIT_FAILURE);

}

void person_print_test(){
	struct Person *p = person_create(NAME, AGE, STATE, DIED);
	//Normal Behavior
	CU_ASSERT_PTR_NOT_NULL(p);
	person_print_a_person(p);
	//Abnormal Behavior
	if (p == NULL){
		CU_ASSERT_PTR_NULL(p);
		person_print_a_person(p);
	}
	CU_PASS("Person printed");
}

void person_state_test(){
	//Normal Behavior
	struct Person *p = person_create(NAME, AGE, STATE, DIED);
	CU_ASSERT_PTR_NOT_NULL(p);
	if (NULL != p){
	CU_ASSERT_STRING_EQUAL(person_get_state(p), "WIDOWED");
	}
	//Abnormal Behavior
	if (p == NULL){
		CU_ASSERT_EQUAL(person_get_state(p), "");
	}
}

void person_find_by_name_test(){
	struct Person *person_first = person_create(NAME, AGE, STATE, DIED);
	struct Person *p = person_first;
	CU_ASSERT_PTR_NOT_NULL(p);
	const char *name ="JACK";
	//Normal Behavior
	if (NULL != p){
		struct Person *person_find_by_name(name,p);
		CU_ASSERT_PTR_NOT_NULL(person_find_by_name);
		CU_ASSERT_STRING_EQUAL(name, p->name);
	}
	//Abnormal Behavior
	if (p == NULL){
		CU_ASSERT_PTR_NULL(p);
		CU_ASSERT_EQUAL(person_find_by_name(name,p),NULL);
	}
	CU_PASS("Person found by name");
}

void person_find_by_age_test(){
	struct Person *person_first = person_create(NAME, AGE, STATE, DIED);
	struct Person *p = person_first;
	CU_ASSERT_PTR_NOT_NULL(p);
	//Normal Behavior
	if(NULL != p){
		struct Person *Person = person_find_by_age(45, p);
		CU_ASSERT_PTR_NOT_NULL(Person);
		CU_ASSERT_EQUAL(45, Person->age);
	}
	//Abnormal Behavior
	if (p == NULL){
		CU_ASSERT_PTR_NULL(p);
		CU_ASSERT_EQUAL(person_find_by_age(45, p), NULL);
		}
	CU_PASS("Person found by age");
}

void person_sort_test(){
	person_bubble_sort(ASCENDING, person_compare_by_age);
	struct Person *currentPerson = person_first();
	struct Person *nextPerson = TAILQ_NEXT(currentPerson, next);
	//Normal Behavior
	while (nextPerson){
		CU_ASSERT_FALSE((currentPerson->age) > (nextPerson->age));
				currentPerson = nextPerson;
				nextPerson = TAILQ_NEXT(nextPerson, next);
	}
	//Abnormal Behavior
	if (person_compare_by_age == NULL){
		CU_ASSERT_PTR_NULL(person_compare_by_age);
		return;
	}
	CU_PASS("Sorted Successfully");
}

int main (int argc, char **argv) {
		CU_pSuite pSuite=NULL;

		if (CUE_SUCCESS!=CU_initialize_registry())
			return (CU_get_error());



		pSuite=CU_add_suite("Suite 1", init_test, clean_test);
		if (NULL==pSuite) {
				CU_cleanup_registry();
				return CU_get_error();
		}


        if ((NULL==CU_add_test(pSuite,	"Create Person", test_create_person)) ||
					(NULL== CU_add_test(pSuite, "Add Person", test_add_person)) ||
					(NULL ==CU_add_test(pSuite, "Get first person", test_person_first)) ||
					(NULL ==CU_add_test(pSuite, "Get last person", test_person_last)) ||
					(NULL == CU_add_test(pSuite, "Load Person from File", test_read_file)) ||
					(NULL == CU_add_test(pSuite, "Print Person", person_print_test)) ||
					(NULL == CU_add_test(pSuite, "Get Person state", person_state_test)) ||
					(NULL == CU_add_test(pSuite, "Find by name", person_find_by_name_test)) ||
					(NULL == CU_add_test(pSuite, "Find by age", person_find_by_age_test)) ||
					(NULL == CU_add_test(pSuite, "Sort Person", person_sort_test))) {
						CU_cleanup_registry();
						return CU_get_error();
        }

		CU_basic_set_mode(CU_BRM_VERBOSE);
		CU_basic_run_tests();
		CU_cleanup_registry();
		return (CU_get_error());
}
