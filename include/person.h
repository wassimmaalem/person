/*
 * main.c
 *
 *  Created on: march 15, 2020
 *      Author: ITSWASSIM
 */

#include <sys/queue.h>

#ifndef PERSON_H_
#define PERSON_H_
/**
 * \brief person implementation with linked lists using TAILQ
 *
 * Implement TAILQ to store every person inside the list
 *
 * \author ITSWASSIM
 *
 * \version 1.0
 *
 * STRICT ensures that all the functions are implemented and if any behaves unexpectedly, returns NULL
 */
#define	STRICT
/**
 * \brief Maximum age of person
 *
 */
#define MAX_AGE 90

/**
 * Description of parameters used to classify Person
 */

/**
 * \enum PersonState
 *
 * Different states that qualify a person's marital status
 */

enum PersonState {
	SINGLE=0,
	MARRIED=1,
	DIVORCED=2,
	WIDOW=3,
	UNKNOWN=4
};

/**
* \enum Ordering
*
*/

enum Ordering {
	ASCENDING,
	DESCENDING
};

/**
 * \brief structure of Person
 *
 * \struct Person
 *
 * \param name of person
 *
 * \param age of person
 *
 * \param state status of person
 *
 * \param died Is the person dead?
 *
 * TAILQ_ENTRY begins the linked list
 */

struct Person	{
	char *name;
	int age;
	enum PersonState state;
	char *died;
	TAILQ_ENTRY(Person) next;
};

/**
 * \brief person_init
 *
 * initialization of person repository
 *
 * \result EXIT_SUCCESS if it is initialized
 */
int person_init();

/**
 * \brief person_deinit module
 *
 * de-initialization of person repository
 *
 * remove person and frees the allocated space
 *
 * \result EXIT_SUCCESS if it is de-initialized
 */
int person_deinit();

/**
 * \brief create a new person
 *
 * \return NULL if there's no name, no died, the age isn't between 0 and the maximum age , the state isn't defined in the enumeration
 *
 * \return result and allocate memory if all the parameters meet the expectation
 */
struct Person *person_create(char *name, int age, int statement, char *died);

/**
 * \brief load a person from the file
 *
 * \return EXIT_FAILURE if there's no name or if the file can't be opened
 *
 * \return EXIT_SUCCESS if the line is allocated memory and the person can be processed and added to the line,
 *
 * after which the line is freed and the file closed
 */
int person_load_from_file(const char *name);

/**
 * \brief add a new person
 *
 * \return EXIT_FAILURE if there's no person and the repository isn't initialized
 *
 * \return EXIT_SUCCESS if the person can be added to the list
 */
int person_add(struct Person *person);

/**
 * \brief remove person
 *
 * \return NULL if the repository isn't initialized
 *
 * \return result if person can be successfully removed from head of repository
 */
struct Person *person_remove_head();

/**
 * \brief get the status of a person
 *
 * \return empty string if there's no person
 *
 * \return status if there is one
 */
char *person_get_state(const struct Person *person);

/**
 * \brief print a person
 *
 * \return if there's no person
 *
 * print the information of one person
 */
void person_print_a_person(const struct Person *person);

/**
 * \brief print all the people on the list
 *
 * \return if the repository isn't initialized
 *
 * print the information of all the people if the repository is initialized
 */
void person_print_all();

/**
 * \brief find a person by their name,
 *
 * \return Null, if there's no name
 *
 * \return the person if it matches the looked for name
 */
struct Person *person_find_by_name(const char *name, struct Person *from);

/**
 * \brief find a person by their age,
 *
 * \return person if the age matches
 *
 * \return NULL if not
 */
struct Person *person_find_by_age(int age, struct Person *from);

/**
 * \brief the first person on the list
 *
 * \return the first person if the repository is initialized
 *
 * \return NULL if it isn't
 */
struct Person *person_first();

/**
 * \brief the last person on the list
 *
 * \return the last person on the list if the repository is initialized annd can be found
 *
 * \return NULL if not
 */
struct Person *person_last();

/**
 * \brief compare 2 people by their names
 *
 * \return 0 if they are equal
 *
 * \return -1 if a is not b
 *
 * \return 1 if b is a
 *
 * \return compared name
 */
int compare_by_name(struct Person *a, struct Person *b);

/**
 * \brief sort 2 individuals using their data and put them in the right order
 *
 * The Bubble sort will switch their places if they aren't in the right order.
 *
 * \return if comparer is NULL
 */
void person_bubble_sort(enum Ordering ordering,
						int (*comparer)(struct Person *a, struct Person *b));

/**
 * \brief compare the people by name
 *
 * \return 0 if they are equal
 *
 * \return -1 if a is not b
 *
 * \return 1 if b is a
 *
 * \return compared name
 */
int person_compare_by_name(const struct Person *a, const struct Person *b);

/**
 * \brief compare by age
 *
 * \return 0 if they are equal
 *
 * \return -1 if a is not b
 *
 * \return 1 if b is a
 *
 * \return (a's age) - (b's age)
 */
int person_compare_by_age(const struct Person *a, const struct Person *b);

/**
 * \brief compare by state
 *
 * \return 0 if they are equal
 *
 * \return -1 if a is not b
 *
 * \return 1 if b is a
 *
 * \return (a's state) - (b's state)
 */
int person_compare_by_state(const struct Person *a, const struct Person *b);

/**
 * \brief compare by died
 *
 * \return 0 if they are equal
 *
 * \return -1 if a is not b
 *
 * \return 1 if b is a
 *
 * \return compared name
 */
int person_compare_by_died(const struct Person *a, const struct Person *b);

/**
 * \brief the number of people entered
 *
 * \return 0, if the repository isn't initialized
 *
 * \return result if the number of people is done counting
 */
int person_counts();

#endif /* PERSON_H_ */
